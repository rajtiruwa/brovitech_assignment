import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity,
    Image,
    Alert 
  } from "react-native";
  
  import React, { useState } from "react";

  
  const login = () => {
    const [username, setUserName] = useState("raj");
    const [password, setPassword] = useState("12345");

    const Onsubmit = () => {
     if(username === "raj" && password === "12345")
     {
      Alert.alert("Login Successfull")
     }
     else
     {
      Alert.alert("Please Check Email & Passowrd")
     }
    };

    return (
      <View style={styles.mainContainer}>
        <View style={styles.headerStyle}>
        
        <Text style={styles.mainHeader}>Login</Text>
        <Text style={styles.SubHeader}>Sign UP</Text>
        </View>
        
        <View style={styles.inputConatiner}>
          <Text style={styles.label}>Email Address</Text>
          <TextInput
            style={styles.inputstyle}
            autoCapitalize="none"
            autoCorrect={false}
            value={username}
            onChange={(actualData)=> setUserName(actualData)}
          />
        </View>
  
        <View style={styles.inputConatiner}>
          <Text style={styles.label}>Password</Text>
          <TextInput
            style={styles.inputstyle}
            autoCapitalize="none"
            autoCorrect={false}
            secureTextEntry={true}
            value={password}
            onChange={(actualData)=> setPassword(actualData)}
          />
        </View>
  
        <View style={styles.wrapper}>
  
          <Text style={styles.wrapperText}>
            Forget Password ?
          </Text>
        </View>
  
        <View>
          <TouchableOpacity onPress={() => Onsubmit()}
            style={styles.buttonStyle}>
            <Text style={styles.buttonText}>Login</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };
  
  export default login;
  
  const styles = StyleSheet.create({
    logo: {
      width: 66,
      height: 58,  
    },
    mainContainer: {
      backgroundColor: "#EDEDED",
      paddingTop: 50,
      height: "100%",
      paddingHorizontal: 0,
      width: "100%"
    },
  
    mainHeader: { 
      fontSize: 25,
      color: "#344055",
      fontWeight: "600",
      paddingTop: 20,
      paddingBottom: 15,
      textTransform: "capitalize",
      marginLeft: 79,
      
    },

    SubHeader: {
      paddingTop: 20,
      paddingBottom: 15,
      fontSize: 25,
      color: "#344055",
      fontWeight: "600",
      textTransform: "capitalize",
      marginLeft: 89,
      backgroundColor: "white",
      
    },

    headerStyle: {
      flexDirection: "row",
      alignItems: "center",
      alignContent: "space-between",
      marginTop: 0,
      backgroundColor: "white",
      height: "40%",
      width: '110%',
      borderRadius: 10,
      paddingTop: 260,
    },
  
    description: {
      fontSize: 20,
      color: "#7d7d7d",
      paddingBottom: 20,
      lineHeight: 25,
      marginLeft: 17,
    },
  
    inputConatiner: {
      paddingHorizontal: 10,
      borderWidth: 0,
      borderColor: "rgba(0,0,0,0.3)",
      paddingHorizontal: 15,
      paddingVertical: 7,
      borderRadius: 1,
      fontSize: 18,
      marginBottom: 1,
      marginTop: 20,
    },
  
    label: {
        color: "#000000",
      flexDirection: "row",
      alignItems: "center",
    },
  
    inputstyle: {
      borderWidth: 0,
      borderBottomWidth: 1,
      borderColor: "rgba(0,0,0,0.3)",
      paddingHorizontal: 15,
      paddingVertical: 19,
      borderRadius: 1,
      fontSize: 15,
      marginTop: 5,
    },
  
    wrapper: {
      flexDirection: "row",
      alignItems: "center",
      marginTop: 0,
      marginLeft: 17,
    },
  
    wrapperText: {
      marginLeft: 5,
      color: "#FA4A0C"
    },
  
    buttonStyle: {
      backgroundColor: "#FA4A0C",
      marginTop: 30,
      borderRadius: 40,
      height: 70,
      justifyContent: "center",
      alignItems: "center",
      width: "80%",
      marginLeft: 45,
    },
  
    buttonText: {
      color: "#fff",
      fontSize: 20,
      justifyContent: "center",
      alignContent: "center",
      fontWeight: "600",
    },
  });
  